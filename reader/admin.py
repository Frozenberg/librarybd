from django.contrib import admin

from .models import (
    Reader,
    ReaderTicket,
    ReaderTicketRecord,
)


@admin.register(Reader)
class ReaderAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'name', 'surname')


# @admin.register(ReaderTicket)
# class TicketAdmin(admin.ModelAdmin):
#     list_display = ('registration_date', 'reader')
#     readonly_fields = ('reader',)

admin.site.register(ReaderTicket)
admin.site.register(ReaderTicketRecord)
