from django.db import models
from django.contrib.auth.admin import User

from book.models import Book

class ReaderTicket(models.Model):
    registration_date = models.DateTimeField(auto_now=True, verbose_name='Дата регистрации')
    expiry_date = models.DateTimeField(null=True, verbose_name='Окончание срока действия')

    class Meta:
        verbose_name_plural = "Читательские билеты"
        verbose_name = "Читательский билет"

    def __str__(self):
        # return str(self.reader)
        return str(self.id)

class ReaderTicketRecord(models.Model):
    date_of_issue = models.DateTimeField(auto_now=True, verbose_name='Дата выдачи книги')
    return_date = models.DateTimeField(verbose_name='Дата возврата')

    book = models.ForeignKey(Book, verbose_name='Книга')
    reader_ticket = models.ForeignKey(ReaderTicket, verbose_name='Читательский билет')

    class Meta:
        verbose_name_plural = "Записи в ЧБ"
        verbose_name = "Запись в ЧБ"

    def __str__(self):
        return str(self.book)

class Reader(models.Model):
    user = models.ForeignKey(User, verbose_name='Пользователь')
    name = models.CharField(max_length=50, verbose_name='Имя')
    last_name = models.CharField(max_length=50, verbose_name='Фамилия')
    surname = models.CharField(max_length=50, verbose_name='Отчество')
    birth_date = models.DateField(verbose_name='Дата рождения')

    passport_series = models.PositiveIntegerField(verbose_name='Серия паспорта')
    passport_number = models.PositiveIntegerField(verbose_name='Номер паспорта')

    ticket = models.ForeignKey(ReaderTicket, verbose_name='Читательский билет')

    avatart = models.ImageField(null=True, verbose_name='Фотография')

    class Meta:
        verbose_name_plural = "Читатели"
        verbose_name = "Читатель"

    def __str__(self):
        return '{} {}. {}.'.format(self.last_name, self.name[0], self.surname[0])
