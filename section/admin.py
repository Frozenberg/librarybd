from django.contrib import admin

from .models import (
    Librarian,
    BookCase,
    Shelf,
    Section
)

@admin.register(Librarian)
class LibrarianAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'name', 'surname')

admin.site.register(BookCase)
admin.site.register(Shelf)
admin.site.register(Section)