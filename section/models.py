from django.db import models

from django.contrib.auth.admin import User

class Section(models.Model):
    title = models.CharField(max_length=100, verbose_name='Название')

    class Meta:
        verbose_name_plural = "Отделы"
        verbose_name = "Отдел"

    def __str__(self):
        return self.title

class Librarian(models.Model):
    user = models.ForeignKey(User, verbose_name='Пльзователь')

    name = models.CharField(max_length=50, verbose_name='Имя')
    last_name = models.CharField(max_length=50, verbose_name='Фамилия')
    surname = models.CharField(max_length=50, verbose_name='Отчество')
    passport_series = models.PositiveIntegerField(verbose_name='Серия паспорта')
    passport_number = models.PositiveIntegerField(verbose_name='Номер паспорта')

    work_shift = models.CharField(max_length=50, verbose_name='Смена')
    work_time_start = models.TimeField(verbose_name='Время начала работы')
    work_time_end = models.TimeField(verbose_name='Время окончания работы')

    section = models.ForeignKey(Section, verbose_name='Отдел')
    class Meta:
        verbose_name_plural = "Библиотекари"
        verbose_name = "Библиотекарь"

    def __str__(self):
        return '{} {}. {}.'.format(self.last_name, self.name[0], self.surname[0])

class BookCase(models.Model):
    section = models.ForeignKey(Section)

    class Meta:
        verbose_name_plural = "Шкафы"
        verbose_name = "Шкаф"

    def __str__(self):
        return str(self.id)


class Shelf(models.Model):
    book_case = models.ForeignKey(BookCase)

    class Meta:
        verbose_name_plural = "Полки"
        verbose_name = "Полка"

    def __str__(self):
        return str(self.id)
