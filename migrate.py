from subprocess import call

commands = [
    'python manage.py makemigrations',
    'python manage.py migrate',
    'python manage.py makemigrations section',
    'python manage.py migrate section',
    'python manage.py makemigrations book',
    'python manage.py migrate book',
    'python manage.py makemigrations reader',
    'python manage.py migrate reader',
]

for command in commands:
    call(command.split())

