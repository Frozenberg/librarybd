CREATE DATABASE librarybd;
CREATE USER librarybd WITH PASSWORD 'librarybd';
ALTER ROLE librarybd SET client_encoding TO 'utf8';
ALTER ROLE librarybd SET default_transaction_isolation TO 'read committed';
ALTER ROLE librarybd SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE librarybd TO librarybd;