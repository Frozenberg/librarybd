from django.contrib import admin


from .models import (
    Book,
    Condition,
    Country,
    Genre,
    Author,
    Publisher,
)


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'isbn', 'date_published')


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('last_name', 'name', 'surname')


admin.site.register(Condition)
admin.site.register(Country)
admin.site.register(Genre)
admin.site.register(Publisher)
