from django.db import models

from section.models import Shelf

class ModelWithName(models.Model):
    name = models.CharField(max_length=50, verbose_name='Название')

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Genre(ModelWithName):
    class Meta:
        verbose_name_plural = "Жанры"
        verbose_name = "Жанр"

class Condition(ModelWithName):
    class Meta:
        verbose_name_plural = "Состояния книг"
        verbose_name = "Состояние книги"


class Country(ModelWithName):
    class Meta:
        verbose_name_plural = "Страны"
        verbose_name = "Страна"


class Publisher(ModelWithName):
    class Meta:
        verbose_name_plural = "Издательства"
        verbose_name = "Издательство"


class Author(models.Model):
    name = models.CharField(max_length=50, verbose_name='Имя')
    last_name = models.CharField(max_length=50, verbose_name='Фамилия')
    surname = models.CharField(max_length=50, verbose_name='Отчество')
    birth_date = models.DateField(verbose_name='Дата рождения')

    class Meta:
        verbose_name_plural = "Авторы"
        verbose_name = "Автор"

    def __str__(self):
        return '{} {}. {}.'.format(self.last_name, self.name[0], self.surname[0])

class Book(models.Model):
    title = models.CharField(max_length=100, verbose_name='Название')
    isbn = models.CharField(max_length=80, verbose_name='isbn')
    edition_count = models.BigIntegerField(verbose_name='Тираж')
    date_published = models.DateField(verbose_name='Дата выхода')

    condition = models.ForeignKey(Condition, verbose_name='Состояние')
    country = models.ForeignKey(Country, verbose_name='Страна')
    publisher = models.ForeignKey(Publisher, verbose_name='Издательство')

    genre = models.ManyToManyField(Genre, verbose_name='Жанр')
    author = models.ManyToManyField(Author, verbose_name='Автор')

    shelf = models.ForeignKey(Shelf, verbose_name='Полка')

    class Meta:
        verbose_name_plural = "Книги"
        verbose_name = "Книга"

    def __str__(self):
        return '{} {}'.format(self.title, self.isbn)
